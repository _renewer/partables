<?php

namespace app\controllers;

use moonland\phpexcel\Excel;
use Yii;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\UploadXls;


class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new UploadXls();
        if (Yii::$app->request->isPost) {
            $model->xlsFile = UploadedFile::getInstance($model, 'xlsFile');
            if ($model->upload()) {

                $data = Excel::import(__DIR__.'/../uploads/'.$model->xlsFile->name);

                return $this->render('show', compact('data'));
            }
        }
        return $this->render('index', compact('model'));
    }
    public function actionShow(){
        $this->refresh();

        return $this->render('show');
    }
}

