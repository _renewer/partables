<?php

/* @var $this yii\web\View */

$this->title = 'Add file';
?>

<?php
use yii\widgets\ActiveForm;
?>


<div class="upload-file">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'xlsFile')->fileInput() ?>

    <button>Upload</button>
    <?php ActiveForm::end() ?>
</div>
<?php

?>

